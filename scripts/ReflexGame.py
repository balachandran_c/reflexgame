import argparse
import math

from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import osc_message_builder
from pythonosc import udp_client
import threading
import time
import struct
from SerialListener import BaseSerialListener

START_DELAY     = 3
DELAY_STEP      = 0.5
GAME_DUR        = 3 * 60

score = 0

class SerialHandler( BaseSerialListener ) :
    def __init__( self ) :
        super( SerialHandler, self ).__init__()

    def getReadBytes( self ):
        return 2

    def handleData( self, data ) :
        resp = struct.unpack( "<bb", data )
        print( "response", resp )
        if( resp[1] == 0 ):
            print( "Button", resp[0], "pressed")
            cmd = struct.pack( "<bb", resp[0] , 0)
            print( cmd )
            self.sendData( cmd )

            cmd = struct.pack( "<bb", resp[0] + 1, 1)
            print( cmd )
            self.sendData( cmd )

    # For sending data:   
    # data = struct.pack( "<bb", btn, state  )
    # print( data )
    # serialHandler.sendData( data )

if __name__ == '__main__':
    serialHandler = SerialHandler()
    try :
        while True:
                time.sleep(1)
    except KeyboardInterrupt:
        pass

    serialHandler.stop()
