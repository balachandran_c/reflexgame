import sys, os
import time
import argparse
import pickle
import socket

from datetime           import datetime
from threading          import *
from queue              import Queue

from PyQt5.QtCore       import *
from PyQt5.QtGui        import *
from PyQt5.QtQuick      import *
from PyQt5.QtQml        import *
from PyQt5.QtMultimedia import *
from PyQt5.QtPrintSupport import *
from PyQt5.QtWidgets    import *

from pythonosc import osc_message_builder
from pythonosc import udp_client

OSC_PORT = 3000
TCP_PORT = 3001

class UploadWorker( Thread ):
    def __init__( self, ip ):
        super( UploadWorker, self ).__init__()
        self._queue = Queue()
        self._ip = ip
        self.start()

    def run( self ):
        print( "Starting upload worker..." )
        while True:
            cmd, args = self._queue.get()
            if cmd == "quit":
                break
            if cmd == "upload":
                self._upload( args )
        print( "Exiting upload worker." )

    def upload( self, args ):
        self._queue.put( ("upload", args) )

    def _upload( self, args ):
        try:
            data = pickle.dumps( args )
            sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            sock.connect( (self._ip, TCP_PORT) )
            sock.sendall( data )
            print( "sent data: {}".format( len( data ) ) )
        except Exception as e:
            print( "Error uploading: {}".format( e ) )

    def stop( self ):
        self._queue.put( ("quit", {}) )

class Application( QObject ):

    def __init__( self ):
        QObject.__init__( self )
        self.app = QApplication( sys.argv )
        engine = QQmlEngine()
        engine.quit.connect( self.quit )
        self.app.lastWindowClosed.connect( self.quit )
        appDirPath = os.path.dirname( os.path.abspath( __file__ ) )
        rootContext = engine.rootContext()
        rootContext.setContextProperty( 'controller', self )
        rootContext.setContextProperty( 'appPath', appDirPath.replace( "\\", "/" ) + "/" )

        parser = argparse.ArgumentParser()
        parser.add_argument( "-f", action='store_true', help="fullscreen" )
        parser.add_argument( "--ip", type=str, default="127.0.0.1", help="The ip of RGServer machine." )
        args = parser.parse_args()

        # self._uploadWorker = UploadWorker( args.ip )
        # self._photoThresh = args.thresh
        self._ip = args.ip
        self._osc = udp_client.UDPClient(args.ip, OSC_PORT)
        self._datafile = open( os.path.join( appDirPath, 'data.csv' ), 'a' )

        windowComp = QQmlComponent( engine, QUrl( 'qml/main.qml' ) )
        self.window = windowComp.create()
        if not self.window:
            print( "Unable to load qml: {0}".format( windowComp.errorString() ) )
            return
        iconPath = os.path.join( appDirPath, os.path.normcase( './qml/images/ole_icon.ico' ) )
        self.window.setIcon( QIcon( iconPath ) )
        if args.f:
            self.window.showFullScreen()
        else:
            self.window.show()
        self.app.exec()

    @pyqtSlot( str, str, str )
    def register( self, name = "player", email = "email", phone = "phone" ):
        params = [ name, email, phone ]
        print( *params )
        self._datafile.writelines( ",".join( params ) + "\n" )
        msg = osc_message_builder.OscMessageBuilder(address = "/reg")
        for p in params:
            msg.add_arg( p )
        msg = msg.build()
        self._osc.send( msg )

    @pyqtSlot()
    def startGame( self ):
        print( "start" )
        msg = osc_message_builder.OscMessageBuilder(address = "/start_game")
        msg.add_arg( 1.0 )
        msg = msg.build()
        self._osc.send( msg )

    def cleanup( self ):
        # self._uploadWorker.stop()
        self._datafile.close()
        pass

    def quit( self ):
        self.cleanup()
        self.app.quit()

if __name__ == "__main__":
    app = Application()
