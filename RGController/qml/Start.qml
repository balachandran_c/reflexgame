import QtQuick 2.6

Item {
    id: start
    anchors.fill: parent

    property real w: bg.paintedWidth
    property real h: bg.paintedHeight
    property real lm: ( bg.width - w ) / 2.0
    property real tm: ( bg.height - h ) / 2.0

    Image {
        id: bg
        source: "images/start_bg.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        CButton {
            y: tm + ( 400 / 1280 ) * h
            anchors.horizontalCenter: parent.horizontalCenter
            width: ( 350 / 800 ) * w
            height: ( 114 / 1280 ) * h
            source: "images/start"
            onClicked: {
                controller.startGame()
                main.setState( "reg" )
            }
        }
    }
}
