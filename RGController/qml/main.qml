import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls 2.2 as QQ2

ApplicationWindow {
    property real ratio: 0.35
    width: 800 * ratio
    height: 1280 * ratio
    title: "Reflex Game Controller"

    Rectangle {
        id: main
        anchors.fill: parent
        color: "#0E5BA8"
        state: "reg"
        property string cameraDeviceId: ""
        property int cameraOrientation: 0
        property string imagePath: ""

        function setState( newState ) {
            main.state = newState
        }

        FontLoader {
            id: fontRegular
            source: "fonts/regular.ttf"
        }

        FontLoader {
            id: fontBold
            source: "fonts/bold.otf"
        }

        Loader {
            id: pageLoader
            anchors.fill: parent
        }

        states: [
            State {
                name: "reg"
                PropertyChanges { target: pageLoader; source: "Registration.qml" }
            },
            State {
                name: "start"
                PropertyChanges { target: pageLoader; source: "Start.qml" }
            }
        ]
    }
}
