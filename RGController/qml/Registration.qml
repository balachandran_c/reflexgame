import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls 2.2 as QQ2
import QtQuick.Layouts 1.1
import "virtualKeyboard/"

Item {
    id: reg
    anchors.fill: parent

    property real w: bg.paintedWidth
    property real h: bg.paintedHeight
    property real lm: ( bg.width - w ) / 2.0
    property real tm: ( bg.height - h ) / 2.0

    Image {
        id: bg
        source: "images/reg_bg.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        QQ2.TextField {
            id: nameField
            x: lm + ( 250/800 ) * w
            y: tm + ( 266/1280 ) * h
            width: ( 360/800 ) * w
            height: h * ( 40 / 1280 )
            font.family: fontBold.name
            font.pixelSize: h * ( 40 / 1280 )
            font.capitalization: Font.AllUppercase
            background: Item {}
            padding: 0
            verticalAlignment: TextInput.AlignBottom
            onFocusChanged: {
                if( focus )
                    keyboard.input = this
            }
            color: "black"
            KeyNavigation.tab: emailField
            KeyNavigation.down: emailField
        }

        QQ2.TextField {
            id: emailField
            x: lm + ( 250/800 ) * w
            y: tm + ( 350/1280 ) * h
            width: ( 360/800 ) * w
            height: h * ( 40 / 1280 )
            font.family: fontBold.name
            font.pixelSize: h * ( 40 / 1280 )
            font.capitalization: Font.AllUppercase
            background: Item {}
            padding: 0
            verticalAlignment: TextInput.AlignBottom
            onFocusChanged: {
                if( focus )
                    keyboard.input = this
            }
            color: "black"
            KeyNavigation.tab: phoneField
            KeyNavigation.down: phoneField
        }

        QQ2.TextField {
            id: phoneField
            x: lm + ( 250/800 ) * w
            y: tm + ( 432/1280 ) * h
            width: ( 360/800 ) * w
            height: h * ( 40 / 1280 )
            font.family: fontBold.name
            font.pixelSize: h * ( 40 / 1280 )
            font.capitalization: Font.AllUppercase
            background: Item {}
            padding: 0
            verticalAlignment: TextInput.AlignBottom
            onFocusChanged: {
                if( focus )
                    keyboard.input = this
            }
            color: "black"
            KeyNavigation.tab: nameField
            KeyNavigation.down: nameField
        }

        CButton {
            y: tm + ( 576 / 1280 ) * h
            anchors.horizontalCenter: parent.horizontalCenter
            width: ( 200 / 800 ) * w
            height: ( 62 / 1280 ) * h
            source: "images/reg"
            onClicked: {
                if( nameField.text.length === 0 )
                    return
                var email = emailField.text.length > 0 ? emailField.text : " "
                var phone = phoneField.text.length > 0 ? phoneField.text : " "
                controller.register( nameField.text, email, phone )
                main.setState( "start" )
            }
        }
    }

    VirtualKeyboard {
        id: keyboard
        width: parent.width
        height: parent.height * 0.33
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        style: Style {
            col_background: "#fff"
            col_but_border: "#fff"
            grad_but_def: Gradient {
                GradientStop { position: 0; color: "#0E5BA8" }
                GradientStop { position: 1; color: "#0E5BA8" }
            }
            grad_but_esc: Gradient {
                GradientStop { position: 0; color: "#0E5BA8" }
                GradientStop { position: 1; color: "#0E5BA8" }
            }
            grad_but_sticked: Gradient {
                GradientStop { position: 0; color: "#0E5BA8" }
                GradientStop { position: 1; color: "#0E5BA8" }
            }
        }
        input: nameField
    }
}
