#define EI_ARDUINO_INTERRUPTED_PIN
#include <EnableInterrupt.h>

int BTNS[] = { 2, 3, 4, 5, 6, 7, 8, 9 };          // btn pin numbers
int LEDS[] = { 10, 11, 12, A0, A1, A2, A3, A4 };  // pins for LEDs
unsigned long lastInterrupt = 0;
int lastBouncePin = 0;
struct __attribute__((__packed__)) serialParams {
  char id;
  boolean state;
};

void setup() {
  for ( int i = 0; i < 8; i++) {
    pinMode( BTNS[ i ], INPUT_PULLUP );
    enableInterrupt( BTNS[ i ], btnPressed, CHANGE );
    pinMode( LEDS[ i ], OUTPUT );
  }
  Serial.begin(9600);
  delay(1000);
}

void btnPressed() {
  unsigned long now = millis();

  if ( ( lastBouncePin == arduinoInterruptedPin ) && ( (now - lastInterrupt) < 500 ) ) {
    //    lastInterrupt = now;
    return;
  }
  lastInterrupt = now;
  serialParams p;
  p.id = (char) index(arduinoInterruptedPin);
  p.state = digitalRead( arduinoInterruptedPin );
  if ( p.state == LOW ) {
    lastBouncePin = arduinoInterruptedPin;
    lastInterrupt = now;
    Serial.write( (byte *) &p, 2 );
    Serial.flush();
  }
}

int index( int pin ) {
  for ( int i = 0; i < 8; i++ ) {
    if ( BTNS[i] == pin) {
      return i;
    }
  }
  return -1;
}

void loop() {
  while ( Serial.available() ) {
    serialParams param;
    read_params( &param, 2 ); // sizeof( char ) + sizeof( boolean)
    setPin( (int) param.id, (int) param.state );
  }
}

void setPin( int id, int state ) {
  digitalWrite( LEDS[ id ], state );
}

void read_params(void *params , int param_size) {
  while (Serial.available() < param_size) {}
  byte *b = (byte *)  params;
  for (int i = 0; i < param_size; i++) {
    b[i] = Serial.read();
  }
}
