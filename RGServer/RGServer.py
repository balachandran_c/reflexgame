import sys, os
import time
import argparse
import pickle
import socket
import inspect

from datetime           import datetime
from threading          import Thread
from queue              import Queue

from PyQt5.QtCore       import *
from PyQt5.QtGui        import *
from PyQt5.QtQuick      import *
from PyQt5.QtQml        import *
from PyQt5.QtMultimedia import *
from PyQt5.QtPrintSupport import *
from PyQt5.QtWidgets    import *

from pythonosc import osc_message_builder
from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server

from SerialListener import *

OSC_PORT = 3000
TCP_PORT = 3001

class Entry:
    def __init__( self, name, score ):
        self.name  = name
        self.score = score

    def __str__( self ):
        return "{}:{}.".format( self.name, self.score )


class EntryListModel( QAbstractListModel ):
    COLUMNS = ( b'name', b'score' )

    def __init__( self ):
        QAbstractListModel.__init__( self )
        self.tweets = []

    def roleNames( self ):
        return dict( enumerate( self.COLUMNS ) )

    def rowCount( self, parent = QModelIndex() ):
        return len( self.tweets )

    def data( self, index, role ):
        if not index.isValid():
            return None
        tweet = self.tweets[ index.row() ]
        if role == self.COLUMNS.index( b'name' ):
            return tweet.name
        elif role == self.COLUMNS.index( b'score' ):
            return tweet.score

    def get( self, index ):
        return self.tweets[ index ]

    def addEntry( self, entry ):
        position = self.rowCount()
        self.beginInsertRows( QModelIndex(), position, position )
        self.tweets.append( entry )
        self.endInsertRows()
        self.sort()

    def removeTweet( self, tweet ):
        if tweet not in self.tweets:
            return
        index = self.tweets.index( tweet )
        self.beginRemoveRows( QModelIndex(), index, index )
        self.tweets.remove( tweet )
        self.endRemoveRows()

    def removeAtIndex( self, index ):
        if index < 0 or index >= len( self.tweets ):
            return
        tweet = self.tweets[ index ]
        self.beginRemoveRows( QModelIndex(), index, index )
        del self.tweets[ index ]
        self.endRemoveRows()
        return tweet

    def set( self, index, tweet ):
        self.tweets[ index ] = tweet
        self.modelReset.emit()

    def clear( self ):
        numTweets = len( self.tweets )
        if numTweets == 0:
            return
        self.beginRemoveRows( QModelIndex(), 0, numTweets - 1 )
        self.tweets.clear()
        self.endRemoveRows()

    def sort( self ):
        self.tweets.sort( key = lambda x: x.score, reverse = True )
        self.modelReset.emit()


class OscServer( Thread ):
    def __init__( self, app ):
        super().__init__()
        self._app = app
        disp = dispatcher.Dispatcher()
        disp.map( "/reg", self.onReg )
        disp.map( "/start_game", self.onStartGame )
        self._server = osc_server.ThreadingOSCUDPServer( ( "0.0.0.0", OSC_PORT ), disp )
        self.start()

    def onReg( self, addr, name, email, phone ):
        print( addr, name, email, phone )
        self._app.newPlayer.emit( name )

    def onStartGame( self, addr, val ):
        print( addr, val )
        self._app.startGame.emit()

    def run( self ):
        print( "Listening to osc at: {}".format( OSC_PORT ) )
        self._server.serve_forever()
        print( "Exiting osc." )

    def stop( self ):
        self._server.shutdown()


class Pillar( BaseSerialListener ) :
    def __init__( self, app, port, id ) :
        self.app = app
        self.id = id
        super( Pillar, self ).__init__( port )

    def getReadBytes( self ):
        return 2

    def onDataReceived( self, data ) :
        resp = struct.unpack( "<bb", data )
        if resp[1] == 0 :
            button = ( self.id * 8 ) + resp[0]
            print( "Button", button, "pressed")
            self.app.buttonHit.emit( int(button) )

    def switch( self, button, on ) :
        print( "Switching button", button, on )
        cmd = struct.pack( "<bb", button, on )
        self.sendData( cmd )

    def switchOn( self, button ) :
        self.switch( button, 1 )

    def switchOff( self, button ) :
        self.switch( button, 0 )


class Application( QObject ):
    newPlayer = pyqtSignal( str )
    startGame = pyqtSignal()
    buttonHit = pyqtSignal( int )

    def __init__( self ):
        QObject.__init__( self )
        self.app = QApplication( sys.argv )
        engine = QQmlEngine()
        engine.quit.connect( self.quit )
        self.app.lastWindowClosed.connect( self.quit )
        appDirPath = os.path.dirname( os.path.abspath( __file__ ) )
        rootContext = engine.rootContext()
        self._leaders = EntryListModel()
        rootContext.setContextProperty( 'controller', self )
        rootContext.setContextProperty( 'leaders', self._leaders )
        rootContext.setContextProperty( 'appPath', appDirPath.replace( "\\", "/" ) + "/" )

        parser = argparse.ArgumentParser()
        parser.add_argument( "-f", action='store_true', help="fullscreen" )
        args = parser.parse_args()

        self._dataPath = os.path.join( appDirPath, 'leaders.csv' )
        self.loadLeaders()
        self._datafile = open( self._dataPath, 'a' )
        self._osc = OscServer( self )
        self.pillars = []
        arduinos = getSerialPorts()
        for i, ard in enumerate( arduinos ) :
            self.pillars.append( Pillar( self, ard, i ) )

        windowComp = QQmlComponent( engine, QUrl( 'qml/main.qml' ) )
        self.window = windowComp.create()
        if not self.window:
            print( "Unable to load qml: {0}".format( windowComp.errorString() ) )
            return
        iconPath = os.path.join( appDirPath, os.path.normcase( './qml/images/ole_icon.ico' ) )
        self.window.setIcon( QIcon( iconPath ) )
        if args.f:
            self.window.showFullScreen()
        else:
            self.window.show()
        self.app.exec()

    def loadLeaders( self ):
        if not os.path.exists( self._dataPath ):
            return
        lines = open( self._dataPath, 'r' ).readlines()
        self._leaders.clear()
        for line in lines:
            name, score = line.split(",")
            name = name.strip()
            score = int( score.strip() )
            entry = Entry( name, score )
            self._leaders.addEntry( entry )
        self._leaders.sort()

    @pyqtSlot( str, int )
    def recordScore( self, name, score ):
        entry = Entry( name, score )
        self._leaders.addEntry( entry )
        self._datafile.write( "{},{}\n".format( name, score ) )

    @pyqtSlot( int )
    def switchOn( self, button ):
        pillar = int( button / 8 )
        btn = button % 8
        self.pillars[ pillar ].switchOn( btn )

    @pyqtSlot( int )
    def switchOff( self, button ):
        pillar = int( button / 8 )
        btn = button % 8
        self.pillars[ pillar ].switchOff( btn )

    def cleanup( self ):
        self._datafile.close()
        self._osc.stop()
        for pillar in self.pillars :
            pillar.stop()

    def quit( self ):
        self.cleanup()
        self.app.quit()


if __name__ == "__main__":
    app = Application()
