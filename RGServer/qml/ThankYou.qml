import QtQuick 2.6

Item {
    id: start
    anchors.fill: parent

    property real w: bg.paintedWidth
    property real h: bg.paintedHeight
    property real lm: ( bg.width - w ) / 2.0
    property real tm: ( bg.height - h ) / 2.0

    Timer {
        interval: 3500
        running: true
        onTriggered: main.setState( "leader" )
    }

    Image {
        id: bg
        source: "images/thankyou.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        Text {
            anchors.top: parent.top
            anchors.topMargin: tm + (380/1080) * h
            anchors.horizontalCenter: parent.horizontalCenter
            height: (96/1080) * h
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: height
            font.capitalization: Font.AllUppercase
            font.family: fontRegular.name
            color: "#0E5BA8"
            text: main.curPlayer
        }

        Text {
            anchors.top: parent.top
            anchors.topMargin: tm + (570/1080) * h
            anchors.left: parent.left
            anchors.leftMargin: lm + (980/1920) * w
            horizontalAlignment: Text.AlignLeft
            height: (130/1080) * h
            font.pixelSize: height
            font.family: fontScore.name
            color: "#0E5BA8"
            text: main.curScore
        }
    }
}
