import QtQuick 2.6

Item {
    id: button
    property string mode: "normal"
    property string source: ""
    property real inactiveRestOpacity: 1.0
    signal clicked()
    signal pressed()
    property real alpha: 1.0

    Image {
        id: active
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: button.source + "_active.png"
        opacity: 1.0 - alpha
    }

    Image {
        id: inactive
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: button.source + "_inactive.png"
        opacity: alpha
        Behavior on opacity {
            enabled: button.mode === "toggle"
            NumberAnimation {}
        }
    }

    MultiPointTouchArea {
        anchors.fill: parent
        mouseEnabled: true
        onPressed: {
            button.pressed()
            if( button.mode === "normal" ) {
                clickAnimation.start()
            } else if ( button.mode === "toggle" ) {
                button.clicked()
            }
        }
    }

    SequentialAnimation {
        id: clickAnimation
        PropertyAnimation { target: button; property: "alpha"; to: 0.0; duration: 100 }
        PropertyAnimation { target: button; property: "alpha"; to: inactiveRestOpacity; duration: 100 }
        onStopped: button.clicked()
    }
}
