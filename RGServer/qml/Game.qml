import QtQuick 2.6

Item {
    id: game
    anchors.fill: parent

    property real w: bg.paintedWidth
    property real h: bg.paintedHeight
    property real lm: ( bg.width - w ) / 2.0
    property real tm: ( bg.height - h ) / 2.0

    property int timeRemaining: 10
    property int hits: 0
    property int misses: 0

    property int kMaxButtons: 16
    property int curButton: -1
    property bool isHit: false

    state: "ready"

    Timer {
        interval: 1000
        running: game.state === "active"
        repeat: true
        onTriggered: {
            timeRemaining = timeRemaining - 1
            if( timeRemaining < 1 ) {
                game.state = "finished"
            }
        }
    }

    Timer {
        interval: 1000
        running: game.state === "finished"
        onTriggered: {
            main.curScore = hits
            controller.recordScore( main.curPlayer, main.curScore )
            main.setState( "thankyou" )
        }
    }

    Timer {
        id: timerOn
        interval: 500
        running: game.state === "active"
        repeat: true
        onTriggered: {
            game.curButton = Math.floor( Math.random() * kMaxButtons )
            game.isHit = false
            controller.switchOn( game.curButton )
            timerOff.start()
        }
    }

    Timer {
        id: timerOff
        interval: 300
        repeat: false
        triggeredOnStart: false
        onTriggered: {
            controller.switchOff( game.curButton )
            game.curButton = -1
        }
    }

    Image {
        id: bg
        source: "images/blue_bg.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        Text {
            anchors.right: parent.right
            anchors.rightMargin: lm + ( 62/1920 ) * w
            anchors.bottom: parent.bottom
            anchors.bottomMargin: tm + ( 48/1080) * h
            anchors.left: parent.left
            anchors.leftMargin: lm
            height: (96/1080) * h
            horizontalAlignment: Text.AlignRight
            font.pixelSize: height
            font.capitalization: Font.AllUppercase
            font.family: fontRegular.name
            color: "white"
            text: main.curPlayer
        }

        Text {
            id: timeText
            anchors.top: parent.top
            anchors.topMargin: tm + (35/1080) * h
            anchors.left: parent.left
            anchors.leftMargin: lm + (60/1920) * w
            height: (96/1080) * h
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: height
            font.family: fontScore.name
            color: "white"
            text: game.timeRemaining
        }

        Text {
            anchors.left: timeText.left
            anchors.top: timeText.bottom
            height: timeText.height * 0.8
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: height
            font.family: fontScore.name
            color: "white"
            text: "sec"
        }

        Item {
            anchors.top: parent.top
            anchors.topMargin: tm + (267/1080) * h
            anchors.horizontalCenter: parent.horizontalCenter
            width: (1178/1920) * w
            height: (348/1080) * h

            Image {
                anchors.left: parent.left
                fillMode: Image.PreserveAspectFit
                height: parent.height
                source: "images/hit.png"

                Text {
                    anchors.top: parent.bottom
                    anchors.topMargin: (94/1080) * h
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: fontScore.name
                    font.pixelSize: timeText.height
                    color: "white"
                    text: hits
                }
            }
            Image {
                anchors.right: parent.right
                fillMode: Image.PreserveAspectFit
                height: parent.height
                source: "images/miss.png"

                Text {
                    anchors.top: parent.bottom
                    anchors.topMargin: (94/1080) * h
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.family: fontScore.name
                    font.pixelSize: timeText.height
                    color: "white"
                    text: misses
                }
            }
        }
    }

    Component.onCompleted: {
        controller.startGame.connect( function() {
            if( typeof( game ) === 'undefined' )
                return
            game.state = "active"
        })
        controller.buttonHit.connect( function( button ) {
            if( typeof( game ) === 'undefined' )
                return
            if( button === game.curButton ) {
                if( !game.isHit ) {
                    game.hits += 1
                    game.isHit = true
                }
            } else {
                game.misses += 1
            }
        })
    }
}
