import QtQuick 2.6
import QtQuick.Controls 1.4
import QtQuick.Controls 2.2 as QQ2

ApplicationWindow {
    property real ratio: 0.35
    width: 1920 * ratio
    height: 1080 * ratio
    title: "Reflex Game"

    Rectangle {
        id: main
        anchors.fill: parent
        color: "#0E5BA8"
        state: "leader"
        property string curPlayer: ""
        property int curScore: 0

        function setState( newState ) {
            main.state = newState
        }

        onCurPlayerChanged: console.log("np: " + curPlayer)

        FontLoader {
            id: fontRegular
            source: "fonts/regular.ttf"
        }

        FontLoader {
            id: fontBold
            source: "fonts/bold.otf"
        }

        FontLoader {
            id: fontScore
            source: "fonts/score.ttf"
        }

        Loader {
            id: pageLoader
            anchors.fill: parent
        }

        states: [
            State {
                name: "leader"
                PropertyChanges { target: pageLoader; source: "Leader.qml" }
            },
            State {
                name: "game"
                PropertyChanges { target: pageLoader; source: "Game.qml" }
            },
            State {
                name: "thankyou"
                PropertyChanges { target: pageLoader; source: "ThankYou.qml" }
            }
        ]

        Component.onCompleted: {
            controller.newPlayer.connect( function( player ) {
                if( main.state !== "leader" )
                    return
                main.curPlayer = player
                main.setState( "game" )
            } )
        }
    }
}
