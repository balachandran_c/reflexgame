import QtQuick 2.6
import QtQuick.Controls 1.4

Item {
    id: start
    anchors.fill: parent

    property real w: bg.paintedWidth
    property real h: bg.paintedHeight
    property real lm: ( bg.width - w ) / 2.0
    property real tm: ( bg.height - h ) / 2.0

    Image {
        id: bg
        source: "images/leaderboard.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        ListView {
            id: leaderList
            x: lm + ( 85 / 1920 ) * w
            y: tm + ( 361 / 1080 ) * h
            width: ( 1775 / 1920 ) * w
            height:( 677 / 1080 ) * h
            model: leaders
            delegate: leaderDlg
            spacing: 1
            clip: true
        }

        Component {
            id: leaderDlg
            Row {
                width: leaderList.width
                height: ( 66 / 1080 ) * h
                Text {
                    height: parent.height
                    width: (360/1920) * w
                    text: ( index + 1 )
                    font.pixelSize: height * 0.85
                    font.family: fontRegular.name
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    height: parent.height
                    width: (822/1920) * w
                    text: name
                    font.pixelSize: height * 0.85
                    font.family: fontBold.name
                    font.capitalization: Font.AllUppercase
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    height: parent.height
                    width: (592/1920) * w
                    text: score
                    font.pixelSize: height * 0.85
                    font.family: fontRegular.name
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                }
            }

        }
    }
}
